# BRScanKit

[![CI Status](https://img.shields.io/travis/Frank Hernandez/BRScanKit.svg?style=flat)](https://travis-ci.org/Frank Hernandez/BRScanKit)
[![Version](https://img.shields.io/cocoapods/v/BRScanKit.svg?style=flat)](https://cocoapods.org/pods/BRScanKit)
[![License](https://img.shields.io/cocoapods/l/BRScanKit.svg?style=flat)](https://cocoapods.org/pods/BRScanKit)
[![Platform](https://img.shields.io/cocoapods/p/BRScanKit.svg?style=flat)](https://cocoapods.org/pods/BRScanKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BRScanKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BRScanKit'
```

## Author

Frank Hernandez, hernandez.f@rouninlabs.com

## License

BRScanKit is available under the MIT license. See the LICENSE file for more info.
