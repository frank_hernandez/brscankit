#
# Be sure to run `pod lib lint BRScanKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BRScanKit'
  s.version          = '0.2.0'
  s.summary          = "Pod for the BRLMPrinterKit / Brother's scanner"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "This project is only a Pod for the Brother SDK v#{s.version}"
  s.homepage         = 'https://bitbucket.org/frank_hernandez/brscankit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Frank Hernandez' => 'hernandez.f@rouninlabs.com' }
  s.source           = { :git => 'https://frank_hernandez@bitbucket.org/frank_hernandez/brscankit.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.ios.vendored_frameworks = 'BRScanKit/BRScanKit.framework'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  
end
